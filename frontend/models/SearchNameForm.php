<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * SearchNameForm is the model behind the search GitHub form.
 */
class SearchNameForm extends Model
{
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name is required
            [['name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'GitHub name',
        ];
    }
}
