<?php

/* @var $this yii\web\View */

$this->title = 'Repositories';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Repositories of <?php echo $owner; ?></h1>
    </div>

    <div class="body-content">
        <?php foreach ($model as $repo): ?>
            <div>
                <a href="<?php echo $repo['clone_url'] ?>"><?php echo $repo['name'] ?></a>
            </div>
        <?php endforeach ?>
    </div>
</div>
