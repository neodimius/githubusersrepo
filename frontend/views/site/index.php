<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'GitHub Users Repo';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Search on GitHub</h1>

        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'options' => ['enctype' => 'multipart/form-data'],
            'action' => Url::to(['site/search-name']),
        ]) ?>
        
            <?= $form->field($model, 'name') ?>
            <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
        
        <?php ActiveForm::end() ?>
    </div>

    <div class="body-content">

        <div class="row">
            
        </div>

        <?php if(isset($repositories)): ?>
            <?php if(empty($repositories)): ?>
                <div class="bg-danger">Repositories not found</div>
            <?php else: ?>
                <div class="jumbotron">
                    <h2>Repositories of <?php echo $owner; ?>:</h2>

                    <?php foreach ($repositories as $repo): ?>
                        <div>
                            <a href="<?php echo $repo['clone_url'] ?>" target="blank"><?php echo $repo['name'] ?></a>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
        <?php endif ?>

    </div>
</div>
